# JUnit 5  Getting Started Story
**To read**: [https://gitlab.com/tair.mustafayev/junit5-getting-started-story-java]

**Estimated reading time**: 15 minutes

## Story Outline
This is a story about usage of JUnit 5 in your Spring Boot application.
It's a short getting started guide to understand which dependencies to use and how to write your first JUnit 5 test!
Here you can also find few examples of usage JUnit + Mockito and some tips and tricks of JUnit 5 features.

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #junit #junit5 #springboot #spring