package io.codingstories.junit5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GettingStartedStoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(GettingStartedStoryApplication.class, args);
	}

}
